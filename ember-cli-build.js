'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const autoprefixer = require('autoprefixer');
const isProductionLike = true;

module.exports = function(defaults) {
    let app = new EmberApp(defaults, {
        // Add options here
        babel: {
        },
        "ember-cli-babel": {
            compileModules: true,
            includePolyfill: true
        },
        postcssOptions: {
            compile: {
                enabled: false,
                browsers: ['last 3 versions'], // this will override config found in config/targets.js
            },
            filter: {
                enabled: true,
                plugins: [
                    {
                        module: autoprefixer,
                        options: {
                          browsers: ['last 2 versions'] // this will override the config, but just for this plugin
                        }
                    }
                ]
            }
        },
        sourcemaps: {
                enabled: !isProductionLike
        },
        minifyCSS: {enabled: isProductionLike},
        minifyJS: {enabled: isProductionLike},
    });

	return app.toTree();
};
