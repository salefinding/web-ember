import DS from 'ember-data';
import config from '../config/environment';

export default DS.RESTAdapter.extend({
	// Application specific overrides go here
	host: config.api_host,
	namespace: ''
});