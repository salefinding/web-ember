import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

export default DS.Model.extend({
    stock: DS.attr('number'),
    createdAt: DS.attr('date'),
    uuid: DS.attr('string'),
    status: DS.attr('boolean'),
    colorCode: DS.attr('string'),
    sizeCode: DS.attr('string'),
    itemDetail: belongsTo('item-detail'),
    item: belongsTo('item'),
    itemDetailId: DS.attr('string')
});