import DS from 'ember-data';
const { Model, belongsTo } = DS;

export default Model.extend({
	brand: belongsTo('brand'),
	cat1: DS.attr('string'),
	cat2: DS.attr('string'),
	cat3: DS.attr('string'),
	cat4: DS.attr('string'),
	cat5: DS.attr('string'),
});