import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

export default DS.Model.extend({
    item: belongsTo('item'),
    thumbUrl: DS.attr('string'),
    url: DS.attr('string'),
    uuid: DS.attr('string'),
    createdAt: DS.attr('date'),
    updatedAt: DS.attr('date'),
});