import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';
import { get, computed } from '@ember/object';

export default DS.Model.extend({
    price: DS.attr('string', { default: '0' }),
    oldPrice: DS.attr('string', { default: '0' }),
    createdAt: DS.attr('date'),
    url: DS.attr('string'),
    code: DS.attr('string'),
    uuid: DS.attr('string'),
    countryName: DS.attr('string'),
    item: belongsTo('item'),
    stock: belongsTo('stock'),
    image: DS.attr('string'),

    currentPrice: computed('oldPrice', function() {
        const price = get(this, 'price');
        if(!price) {
            return '0';
        }
        
        return price;
    }),

    oldCurrentPrice: computed('oldPrice', function() {
        const oldPrice = get(this, 'oldPrice');
        if(!oldPrice) {
            return get(this, 'currentPrice');
        }
        
        return oldPrice;
    }),
});