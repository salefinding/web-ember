import DS from 'ember-data';
import { hasMany } from 'ember-data/relationships';
import { get, computed } from '@ember/object';

export default DS.Model.extend({
	name: DS.attr('string'),
	price: DS.attr('string', { default: '0' }),
	maxSaleRate: DS.attr('number'),
	available: DS.attr('boolean'),
	itemDetails: hasMany('item-detail'),
	images: hasMany('images'),
	code: DS.attr('string'),
	stocks: hasMany('stock'),

	currentPrice: computed('oldPrice', function() {
        const price = get(this, 'price');
        if(!price) {
            return '0';
        }
        
        return price;
    }),
});