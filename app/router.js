import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
	location: config.locationType,
	rootURL: config.rootURL
});

Router.map(function() {
	this.route("items", function() {
		this.route('detail', {path: '/:item_id'});
	});
	this.route("cart", function() {
	});
	this.route('profile');
	this.route('main');
});

export default Router;
