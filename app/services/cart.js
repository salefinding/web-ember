import Service from '@ember/service';
import { on } from '@ember/object/evented';
import { set, get, observer, computed } from '@ember/object';

export default Service.extend({
    listItems: [],
    totalItem: 0,

    addItem(item) {
        const quantity = get(item, 'quantity') || 0;
        
        if(quantity > 0) {
            set(item, 'quantity', quantity + 1);
        } else {
            set(item, 'quantity', 1);
        }

        const subtotal = quantity * item.price || 0;

        set(item, 'subtotal', subtotal);
        if(quantity === 0) {
            get(this, 'listItems').push(item);
        }

        const length = get(this, 'listItems.length');
        set(this, 'totalItem', length);
    },

    total: computed('listItems.@each.{quantity,subtotal}', function() {
        return get(this, 'listItems').reduce((prevVal, item) => prevVal + item.subtotal, 0);
    }),

    _setupCart: on('init', function() {
		// Load cart from server 
	}),
});