import Route from '@ember/routing/route';
import { get } from '@ember/object';
import RSVP from 'rsvp';

export default Route.extend({
	beforeModel(transition) {
		this._super(...arguments);

		if (transition.targetName === 'index') {
			this.transitionTo('main');
		}
	},

    model() {
		return RSVP.hash({
			brands: get(this, 'store').findAll('brand')
		});
	},

	setupController(controller, model) {
		controller.setProperties(model);
	},
});
