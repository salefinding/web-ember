import { later } from '@ember/runloop';
import Component from '@ember/component';
import { computed, observer, set, get } from '@ember/object';

export default Component.extend({
	classNames: ['num-input'],
	classNameBindings: ['hasPrefix', 'hasSuffix', 'hasAllButton:num-input--with-all'],
	hasPrefix: computed('prefix', function() {
		return get(this, 'prefix') !== '';
	}),
	hasSuffix: computed('suffix', function() {
		return get(this, 'suffix') !== '';
	}),
	prefix: '', // for $ % etc
	suffix: '',
	value: '0',
	previousValue: '0',
	increment: 1,
	maxDecimals: 4,
	max: Number.MAX_SAFE_INTEGER,
	min: Number.MIN_SAFE_INTEGER,
	cursorPos: '0',
	hasAllButton: false,
	allButtonText: 'All',
	whitelistKeys: [
		8, 9, // delete, tab
		37, 38, 39, 40, // arrow keys
		48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, // numbers 0-9
		189, 190 // dash, dot
	],
	ctrlPressed: false,
	cmdPressed: false,
	altPressed: false,
	shiftPressed: false,

	filterInput: observer('value', function() {
		let value = get(this, 'value').toString();
		let previousValue = get(this, 'previousValue');
		if(!this.validInput(value, get(this, 'max'), get(this, 'min'))) {
			this.sendAction('error');
			later(() => {
				set(this, 'value', previousValue); //rejected
			});
		} else {
			set(this, 'previousValue', value);
			this.formatLeadingZeros();
			this.round();
		}
	}),
	actions: {
		increase: function() {
			if((get(this, 'max') === null) || ((parseInt(get(this, 'value'), 10) + get(this, 'increment')) <= get(this, 'max'))) {
				this.incrementProperty('value', get(this, 'increment'));
			} else if((get(this, 'value') === '') || (get(this, 'value') === '-')) {
				set(this, 'value', 1);
			}
			this.triggerChange();

			this.$('.num-input__button--up').addClass('num-input__button--highlight');
			setTimeout(function() {
				this.$('.num-input__button--up').removeClass('num-input__button--highlight');
			}, 100);
			this.$('.num-input__input').focus();
		},
		decrease: function() {
			// REMOVE TEMPOTARY NEED REOPEN WHEN HAVE ISSUE RELATE TO KEY INPUT
			/*if (this.$().has(document.activeElement).length === 0) {
				return; // this fixes a bug where pressing enter in another field within the form would activate this action for some reason
			}*/
			if((get(this, 'min') === null) || ((parseInt(get(this, 'value'), 10) - get(this, 'increment')) >= get(this, 'min'))) {
				this.decrementProperty('value', get(this, 'increment'));
			} else if((get(this, 'value') === '') || (get(this, 'value') === '-')) {
				set(this, 'value', '-1');
			}
			this.triggerChange();

			this.$('.num-input__button--down').addClass('num-input__button--highlight');
			setTimeout(function() {
				this.$('.num-input__button--down').removeClass('num-input__button--highlight');
			}, 100);
			this.$('.num-input__input').focus();
		},
		setMax() {
			set(this, 'value', get(this, 'max'));
			this.triggerChange();
		}
	},

	triggerChange() {
		let onchange = get(this, 'onchange');
		if(onchange) {
			onchange(get(this, 'value'));
		}
	},

	didInsertElement() {
	},

	keyDown(e) {
		let value = get(this, 'value').toString();
		let whitelist = get(this, 'whitelistKeys');
		let maxDecimals = get(this, 'maxDecimals');
		let cursorPos = null;

		let k = e.keyCode;
		switch(k) {
			case 91: /* cmd/super */
				set(this, 'cmdPressed', true);
				break;
			case 18: /* alt/option */
				set(this, 'altPressed', true);
				break;
			case 17: /* ctrl */
				set(this, 'ctrlPressed', true);
				break;
			case 16: /* shift */
				set(this, 'shiftPressed', true);
				break;
		}
		if(!(get(this, 'ctrlPressed') || get(this, 'cmdPressed') || get(this, 'altPressed') || get(this, 'shiftPressed')) && // a modifier is not pressed
			(whitelist.indexOf(k) === -1) // if it's not in the whitelist
		) {
			e.preventDefault();
		} else if((value.indexOf('.') !== -1) && (value.split('.')[1].length >= maxDecimals) && ((value.length - e.target.selectionStart) <= maxDecimals) && ([48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58].indexOf(k) !== -1)) {
			e.preventDefault(); // prevent typing too many decimals
		} else if(([49, 50, 51, 52, 53, 54, 55, 56, 57, 58].indexOf(k) !== -1) && (value.match(/^-0\./)) && e.target.selectionStart === 2) {
			cursorPos = 2; // handle the special case  pressing a number at cursor position 2 on -0.
		} else if(([49, 50, 51, 52, 53, 54, 55, 56, 57, 58].indexOf(k) !== -1) && (value.match(/^0\./)) && e.target.selectionStart === 1) {
			cursorPos = 1; // handle the special case pressing a number at position 1 on 0.
		} else { // handle each case individually
			switch(k) {
				case 8: // delete key
					if((e.target.selectionStart === 2 && value.match(/^0\../)) || ((e.target.selectionStart === 3) && (value.match(/^-0\../)))) {
						this.setCursor(e, e.target.selectionStart - 2);
					} else if(e.target.selectionStart === 1 && value.match(/^-\./)) {
						cursorPos = 2;
					} else if(e.target.selectionStart === 1 && value.match(/^-/)) {
						cursorPos = 0;
					} else if(e.target.selectionStart === 1 && value.match(/^[1-9]\./)) {
						cursorPos = 1;
					} else if(e.target.selectionStart === 2 && value.match(/^-[1-9]\./)) {
						cursorPos = 2;
					}
					break;
				case 38: // up key
					this.send('increase');
					this.setCursor(e, e.target.selectionStart);

					break;
				case 40: // down key
					this.send('decrease');
					this.setCursor(e, e.target.selectionStart);

					break;
				case 48: // check for leading 0
					if((e.target.selectionStart === e.target.selectionEnd) && (
							(e.target.selectionStart === 0 && value !== '') ||
							(e.target.selectionStart === 1 && value.match(/^-/)) ||
							(e.target.selectionStart === 1 && value.match(/^0/)) ||
							(e.target.selectionStart === 2 && value.match(/^-0/)))
					) {
						e.preventDefault();
					}
					break;
				case 189: // dash
					if((e.target.selectionStart !== 0) || value.match(/^-/)) {
						e.preventDefault();
					}
					else if(value.match(/^\./)) {
						cursorPos = 2;
					} // if the value doesn't have it's leading zero for some reason
					break;
				case 190: // decimal
					if((value.indexOf('.') !== -1) || (maxDecimals === 0)) { // if there is already a decimal or if no decimals are allowed
						e.preventDefault();
					} else { // add leading zero  and move the cursor to the appropriate position if applicable.
						if((e.target.selectionStart === 0) || ((e.target.selectionStart === 1) && (value.match(/^-/)))) {
							this.send('formatLeadingZeros');
							this.setCursor(e, e.target.selectionStart + 2);
						}
					}
					break;
			}
		}
		this.formatLeadingZeros();
		this.round();
		if(cursorPos !== null) {
			this.setCursor(e, cursorPos);
		}
	},
	keyUp(e) {
		let k = e.keyCode;
		switch(k) {
			case 91: /* cmd/super */
				set(this, 'cmdPressed', false);
				break;
			case 18: /* alt/option */
				set(this, 'altPressed', false);
				break;
			case 17: /* ctrl */
				set(this, 'ctrlPressed', false);
				break;
			case 16: /* shift */
				set(this, 'shiftPressed', false);
				break;
		}

		let value = get(this, 'value');
		if(isNaN(Number(value))) {
			set(this, 'value', 1);
		}
		let max = get(this, 'max');
		let min = get(this, 'min');
		if(value < min) {
			set(this, 'value', min);
		}
		if(value > max) {
			set(this, 'value', max);
		}

		this.triggerChange();
	},
	validInput(value, max, min) {
		max = parseInt(max, 10);
		min = parseInt(min, 10);
		if((!value.match(/^-?(0\.)?[0-9]*\.?\d*$/)) || (value.match(/.*\..*\..*/))) {
			return false;
		} else if((parseInt(value, 10) > max) || (parseInt(value, 10) < min)) {
			return false;
		} else {
			return true;
		}
	},
	formatLeadingZeros() { // formats for leading zeros etc.
		let value = get(this, 'value').toString();
		let negative = false;
		if(value.match(/^-/)) {
			negative = true;
			value = value.substr(1);
		}
		if(value.match(/^\./)) { // add leading zero when appropriate
			value = '0' + value;
		} else {
			while(value.match(/^0/) && (!value.match(/^0\./)) && (!value.match(/^0$/))) {
				value = value.replace(/^0/, '');
			}
		}
		if(negative) {
			value = '-' + value;
		}
		set(this, 'value', value);
	},
	round() {
		let value = get(this, 'value').toString();
		let maxDecimals = (get(this, 'maxDecimals'));
		let roundingFactor = Math.pow(10, maxDecimals);
		if(value.indexOf('.') !== -1 && (value.split('.')[1].length > maxDecimals)) {
			value = Math.round((parseFloat(value) * roundingFactor)) / roundingFactor;
			set(this, 'value', value.toString());
		}
	},
	setCursor: function(e, pos) {
		later(function() {
			e.target.setSelectionRange(pos, pos);
		});
	}
});
