import Component from '@ember/component';
import { get, set } from '@ember/object';
import $ from 'jquery';

export default Component.extend({
	classNames: ['utility-bar'],
	scroll() {

	},
	didInsertElement() {
		let titleBarHeight = $('.title-bar').outerHeight();
		let $parent = this.$().parent(),
			parentPadding = parseInt($parent.css('paddingTop'));
		let onScroll = () => {
			let offset = this.$().offset();
			if (!get(this, 'fixed') && offset.top <= titleBarHeight) {
				this.$().data('originalOffset', $('.page__content-wrap').scrollTop() - titleBarHeight + this.$().height());
				// save the original padding top and add padding to prevent jitters
				$parent.data('paddingTop', parentPadding)
					.css('paddingTop', this.$().height() + parentPadding);

				this.$().css({
					position: 'fixed',
					top: titleBarHeight,
					left: offset.left,
					width: this.$().parent().width()
				});
				set(this, 'fixed', true);
			} else if (get(this, 'fixed') && $('.page__content-wrap').scrollTop() < this.$().data('originalOffset')) {
				// reset the parents top padding
				$parent.css('paddingTop', $parent.data('paddingTop'));
				this.$().css({
					position: 'relative',
					left: 0,
					top: 0,
					width: 'auto'
				});
				set(this, 'fixed', false);
			}
		};
		$('.page__content-wrap').on('scroll.utilitybar', onScroll);
		$('.page__content-wrap').on('scroll.utilitybar', onScroll);
	},
	willDestroy() {
		$('.page__content-wrap').off('.utilitybar');
		$('.page__content-wrap').off('.utilitybar');
	}
});
