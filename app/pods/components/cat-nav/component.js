import Component from '@ember/component';
import { get, set } from '@ember/object';
import $ from 'jquery';

export default Component.extend({
    listCategories: null,
    currentBrand: null,

    didInsertElement() {
        $('body').click(event => {
            if(!$(event.target).closest('.mega-dropdown').length && !$(event.target).closest('.dropdown').length) {
                if($('.mega-dropdown.open').is(":visible")) {
                    this.send('closeCategory');
                }
            } 
        });

        $('.page__content').scroll(() => {
            if($('.mega-dropdown.open').is(":visible")) {
                this.send('closeCategory');
            }
        });
    },

    actions: {
        toggleNavigation() {
            this.toggleProperty('navigationOpen');
        },

        showCategory(brand) {
            set(this, 'listCategories', []);
            if(get(this, 'currentBrand.name') === get(brand, 'name')) {
                set(this, 'currentBrand', null);
                set(this, 'navigationOpen', false);
                return;
            }
            set(this, 'currentBrand', brand);
            const categories = get(brand, 'categories');
            let listCategories = {
                listCat: []
            };
            categories.forEach(cat => {
                checkCategory(cat, listCategories.listCat, 1);
            });
            
            listCategories = listCategories.listCat.map(cat => cat);
            set(this, 'listCategories', listCategories);
        },

        closeCategory() {
            set(this, 'listCategories', []);
            set(this, 'currentBrand', null);
            set(this, 'navigationOpen', false);
        },
    }
});

function checkCategory(currentCat, listCategories, level) {
    const catName = get(currentCat, 'cat' + level);
    const catId = get(currentCat, 'id');
    const exist = listCategories.find(category => {
        return category.name === catName;
    });
    if(!exist) {
        listCategories.pushObject({
            name: catName,
            id: catId,
            listCat: []
        });
    } 
    if(!catName) {
        return listCategories;
    }

    const index = listCategories.length - 1;
    return checkCategory(currentCat, listCategories[index].listCat, level + 1);
}
