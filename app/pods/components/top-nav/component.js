import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';
import { getOwner } from '@ember/application';

export default Component.extend({
    cart: service(),

    searchItems: task(function * (keyword) {
        yield timeout(500);  

        const url = window.location.href;
        let controller = null;
        if(url.indexOf('main') >= 0) {
            controller = getOwner(this).lookup('controller:main')
        } else {
            controller = getOwner(this).lookup('controller:items.index')
        }

        controller.sendAction('search', keyword);
	}).restartable(),
});
