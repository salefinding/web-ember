import Component from '@ember/component';
import $ from 'jquery';
import { on } from '@ember/object/evented';
import { get } from '@ember/object';

export default Component.extend({
	classNames: ['page__fixed-panel'],
	classNameBindings: ['hasUtilityBar:page__fixed-panel--has-utility-bar'],
	excludedElements: '',

	didInsertElement() {
		$(window).on('resize.fixedPanel', () => {
			let height = $(window).height() - $(".page__content-menu").outerHeight();
			const excludedElements = get(this, 'excludedElements');
			if(excludedElements) {
				get(this, 'excludedElements').split(',').forEach(selector => {
					$(selector).not(this.$().find(selector)).each(function() {
						height -= $(this).outerHeight(true);
					});
				});
			}
			if(($(window).width() < 640 && !get(this, 'auto')) || ($(window).width() < 1024 && get(this, 'mediumAuto'))) {
				height = 'auto';
			}

			this.$().height(height);
		});
		$(window).trigger('resize.fixedPanel');
	},

	removeListeners: on('willDestroyElement', function() {
		if(this.$(window)) {
			this.$(window).off('resize.fixedPanel');
		}
	})
});
