import Route from '@ember/routing/route';
import { get, set } from '@ember/object';
import RSVP from 'rsvp';

export default Route.extend({
    model() {
		return RSVP.hash({
			items: get(this, 'store').query('item', {topSale: true}).then(items => {
				return items.toArray();
			}),
		});
	},

	setupController(controller, model) {
		controller.setProperties(model);
		set(controller, 'pageNumber', 1);
	}
});
