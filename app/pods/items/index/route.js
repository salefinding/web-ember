import Route from '@ember/routing/route';
import { get } from '@ember/object';
import RSVP from 'rsvp';

export default Route.extend({
	queryParams: {
		categoryId: {
			refreshModel: true
		}	
	},

    model(params) {
		return RSVP.hash({
			items: get(this, 'store').query('item', params),
			category: get(this, 'store').findRecord('category', params.categoryId)
		});
	},

	setupController(controller, model) {
		controller.setProperties(model);
	}
});
