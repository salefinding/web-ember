import Controller from '@ember/controller';
import { get, set, computed } from '@ember/object';
import { task } from 'ember-concurrency';
import $ from 'jquery';

export default Controller.extend({
	pageNumber: 1,
	isMoreItems: true,

	groupedItems: computed('items.length', function() {
		const items = get(this, 'items');
		if(items.length <= 0) {
			return [];
		}
		let columns = 4;
		const windowWidth = $(window).width();
		if(windowWidth < 600) {
			columns = 1;
		} else if(windowWidth < 1000) {
			columns = 2;
		} else if(windowWidth < 1400) {
			columns = 3;
		}

		const rows = [];
		let row = [];
		get(this, 'items').forEach((item, idx) => {
			if(idx !== 0 && idx % columns === 0) {
				rows.pushObject(row);
				row = [];
			}

			row.pushObject(item);
		});
		if(rows.length === 0 || row.length > 0) {
			rows.pushObject(row);
		}
		return rows;

	}),

	loadMore: task(function * () {
		if(!get(this, 'isMoreItems')) {
			return;
		}

		const pageNumber = get(this, 'pageNumber') + 1;
		set(this, 'pageNumber', pageNumber);

		const newItems = yield get(this, 'store').query('item', {topSale: true, page: pageNumber});
		get(this, 'items').pushObjects(newItems.toArray());
		set(this, 'isMoreItems', pageNumber >= newItems.meta.total_page);
	}).drop(),
});