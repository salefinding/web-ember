import Controller from '@ember/controller';
import { get, set } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
    cart: service(),
    showImage: null,

    init() {
        this._super(...arguments);
        set(this, 'showImage', get(this, 'item.images.firstObject'));
    },

    actions: {
        addToCart(currentItem) {
            get(this, 'cart').addItem(currentItem);   
        },

        changePicture(image) {
            set(this, 'showImage', image);
        }
    }
});