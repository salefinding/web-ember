import Route from '@ember/routing/route';
import { get } from '@ember/object';
import RSVP from 'rsvp';

export default Route.extend({
    model(params) {
		return RSVP.hash({
			item: get(this, 'store').findRecord('item', params.item_id),
		});
	},

	setupController(controller, model) {
		controller.setProperties(model);
	}
});
